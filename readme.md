# Exercice pratique

Le but de l'exercice est de faire communiquer ensemble un front, un back et un service tiers

Il y a 3 repos:
- `front` qui contiendra le react de l'exercice
- `back` qui contentiendra le backend avec lequel communique le front
- `serviceA` qui n'est pas à toucher, il représente un service tiers que le backend va devoir interroger

## Etape 1 `front`
Créer un formulaire à l’aide de react qui comporte 2 champs `Email` et `Message` avec un bouton de validation d’envoie

## Etape 2 `back`
Créer un serveur http avec nodejs qui recevra sur une route de votre choix les champs du formulaire créés

## Etape 3 `back`
Requeter le serviceA avec les paramêtres suivants et renvoyer au front le status et le code
```
host: 127.0.0.1
port: 9999
path: /genid
method: Post
headers: 'content-type: application/json'
body: json
```

### Inputs
| Name | Description | Type | Required |
|----|-----------|----|--------|
| email | Email reçu depuis le front | String | yes |
| msg | Message reçu depuis le front | String | yes |
| webhookPath | Path du webhoock pour updater le status | String | no |

### Outputs
| Name | Description | Type |
| -----| ----------- | ---- |
| id | Un nombre compris entre 0 et 100000 | Number |
| name | Status name | String |
| code | Status code | Number |


## Etape 4 `back`
 Faire en sorte que le front affiche l'id et le status renvoyé par le back

## Etape 5 `back`

Créer une route POST sur le back, qui recevra les arguments suivants:

| Name | Description | Type |
| -----| ----------- | ---- |
| id | Un nombre compris entre 0 et 100000 | Number |
| name | Status name | String |
| code | Status code | Number |

## Etape 6 `back`
 Modifier la requete envoyé lors de l’étape 3, pour mettre à jour le champs  webhookPath avec la route créé sur l’étape précédente 5
 
## Etape 7 `back`

Faire un `console.log` du payload du webhook
 
## Etape 8 `front`

Mettre à jour la valeur sur le front en fonction du retour du webhook

