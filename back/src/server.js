"use strict"

const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const app = express()

async function main() {

    app.use(morgan('combined'))
    app.use(bodyParser.json({}))
    app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }))
    app.use(require('compression')())


    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", '*')
        res.header('Access-Control-Allow-Methods', '*')
        next()
    })

    const server = app.listen(8080, "0.0.0.0", function () {
        console.log('Server listening on ' + "0.0.0.0" + ':' + 8080)
    })

}

main()