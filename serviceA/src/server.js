"use strict"

const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const app = express()

async function main() {

    app.use(morgan('combined'))
    app.use(bodyParser.json({}))
    app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }))
    app.use(require('compression')())


    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", '*')
        res.header('Access-Control-Allow-Methods', '*')
        next()
    })


    app.post("/genid", genid)
    app.get("/ping", (req,res) => {res.send("pong")})


    const server = app.listen(9999, "0.0.0.0", function () {
        console.log('Server listening on ' + "0.0.0.0" + ':' + 9999)
    })

}

async function genid(req, res){

    try {

        res.setHeader('Content-Type', 'application/json');
        res.append('Cache-Control', "no-store")


        req.body = req.body || {}

        if(!req.body.email) return res.status(460).send({err: "Err: Missing email field"})
        if(!req.body.msg) return res.status(461).send({err: "Err: Missing msg field"})
        
        const status = {
            id: Math.floor(Math.random()*1000000),
            name: "pending",
            code: 10
        }

        res.status(200).json(status)

        if(! req.body.webhookPath) return console.log("Warn: Missing webhookPath") 


        status.name = "done"
        status.code = 20
        triggerWebhoock(req.body.webhookPath, JSON.stringify(status), Math.floor(Math.random()*5000))


    }

    catch {
        res.status(500).send({err: "Internal error"})
        console.log(e)
    }

    req.body = req.body || {}


}


function triggerWebhoock(url, data, delay){
    try{
        setTimeout(function(){post(url, data)}, delay)
    }
    catch(e){
        console.log("Error on triggerWebhoock", e)
    }
}

function post(path, data){
  return new Promise((resolve,reject) => {
    const http = require('http')
    const options = {
      hostname: '127.0.0.1',
      port: 8080,
      path: encodeURI(path),
      method: "POST",
      headers: {
        'Content-Length': data.length,
        'Content-Type':  'application/json'
      }
    }

    const req = http.request(options, res => {resolve()})
    req.on('error', (e) => {
      console.log("Error on post data", e)
    })
    req.write(data)
    req.end()
    console.log("sent:", data)
  })
}



main()